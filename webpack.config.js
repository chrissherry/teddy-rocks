const path = require('path');
const webpack = require('webpack');

const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSass = new ExtractTextPlugin({
    filename: "[name].css",
    disable: process.env.NODE_ENV === "development"
});

module.exports = {
    entry: [
        './build/js/index.js',
        './build/sass/main.scss',
        './build/sass/clashfinder.scss'
    ],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'web/compiled')
    },
    devtool: "source-map",
    devServer: {
        contentBase: './web/compiled',
        hot: true
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            }, {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'url-loader',
                options: {
                    limit: 10000
                }
            },
            {
                test: /\.scss$/,
                use: extractSass.extract({
                    use: [{
                        loader: "css-loader",
                        options: {
                            minimize: true,
                            alias: {
                                "../fonts/bootstrap": "bootstrap-sass/assets/fonts/bootstrap"
                            }
                        }
                    }, {
                        loader: "sass-loader",
                        options: {
                            includePaths: [
                                path.resolve("./node_modules/bootstrap-sass/assets/stylesheets")
                            ]
                        }
                    }, {
                        loader: "resolve-url-loader"
                    }],
                    // use style-loader in development
                    fallback: "style-loader"
                })
            }, {
                test: /\.woff2?$|\.woff$|\.ttf$|\.eot$|\.svg$|\.otf$/,
                use: [{
                    loader: "file-loader"
                }]
            }
        ]
    },
    plugins: [
        extractSass
    ]
};