<?php

use App\Controllers\AboutController;
use App\Controllers\ActivitiesController;
use App\Controllers\GalleryController;
use App\Controllers\HomepageController;
use App\Controllers\InfoController;
use App\Controllers\LineupController;
use App\Controllers\MapController;
use App\Controllers\SignupController;
use App\Controllers\TicketController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

//Request::setTrustedProxies(array('127.0.0.1'));

$app['homepage.controller'] = function() use ($app) {
	return new HomepageController($app);
};
$app['about.controller'] = function() use ($app) {
	return new AboutController($app);
};
$app['ticket.controller'] = function() use ($app) {
	return new TicketController($app);
};
$app['lineup.controller'] = function() use ($app) {
	return new LineupController($app);
};
$app['gallery.controller'] = function() use ($app) {
	return new GalleryController($app);
};
$app['activities.controller'] = function() use ($app) {
	return new ActivitiesController($app);
};
$app['info.controller'] = function() use ($app) {
	return new InfoController($app);
};
$app['signup.controller'] = function() use ($app) {
	return new SignupController($app);
};
$app['map.controller'] = function() use ($app) {
	return new MapController($app);
};

// Common
$app->get('/press/', 'about.controller:getPressPage')->bind('press');
$app->get('/story/', 'about.controller:getStoryPage')->bind('story');
$app->get('/who-we-help/', 'about.controller:getWhoWeHelpPage')->bind('who-we-help');

// 2018
$currentYear = $app['controllers_factory'];
$currentYear->get('/', 'homepage.controller:getIndex')->bind('homepage');
$currentYear->get('/get-involved/', 'info.controller:getGetInvolvedPage')->bind('get-involved');
$currentYear->get('/donate/', 'info.controller:getDonatePage')->bind('donate');
$currentYear->get('/tickets/', 'ticket.controller:getTicketPage')->bind('tickets');
$currentYear->get('/tickets/locals', 'ticket.controller:getLocalsTicketPage')->bind('ticket-locals');

$currentYear->get('/lineup/', 'lineup.controller:getLineup')->bind('lineup');
$currentYear->post('/lineup/favourite/', 'lineup.controller:saveFavourite')->bind('clashfinder.favourite');
$currentYear->get('/clashfinder/', 'lineup.controller:getClashFinder')->bind('clashfinder');
$currentYear->get('/clashfinder/json', 'lineup.controller:getLineupJson')->bind('clashfinder.json');

//$currentYear->get('/admin/map/watch', 'map.controller:getMapWatch')->bind('map.watch');
//$currentYear->post('/map/send', 'map.controller:sendEvent')->bind('map.send');
//$currentYear->get('/map/', 'map.controller:getMap')->bind('map');
$currentYear->get('/volunteers/map/', 'map.controller:getBackstageMap')->bind('map.volunteer');
$currentYear->get('/activities/', 'activities.controller:getActivitiesPage')->bind('activities');
$currentYear->get('/info/', 'info.controller:getInfoPage')->bind('info');

$currentYear->get('/play', 'signup.controller:getSignupPlayPage')->bind('play');
$currentYear->post('/play', 'signup.controller:signupPlay')->bind('post-play');
$currentYear->get('/volunteer', 'signup.controller:getSignupVolunteerPage')->bind('volunteer');
$currentYear->post('/volunteer', 'signup.controller:signupVolunteer')->bind('post-volunteer');
$currentYear->get('/trade', 'signup.controller:getSignupTradePage')->bind('trade');
$currentYear->post('/trade', 'signup.controller:signupTrade')->bind('post-trade');
$currentYear->get('/sponsor', 'signup.controller:getSignupSponsorPage')->bind('sponsor');
$currentYear->post('/sponsor', 'signup.controller:signupSponsor')->bind('post-sponsor');
//$currentYear->get('/gallery', 'gallery.controller:getGallery');
//$currentYear->get('/gallery/backstage', 'gallery.controller:getBackstageGallery');


$app->mount('/', $currentYear);

// Admin Stuff
$admin = $app['controllers_factory'];
$admin->get('/artists', 'signup.controller:getPlayList');
$admin->get('/volunteers', 'signup.controller:getVolunteerList');
$admin->get('/traders', 'signup.controller:getTraderList');
$admin->get('/sponsors', 'signup.controller:getSponsorList');
$app->mount('/admin', $admin);

// Archive
$archive = $app['controllers_factory'];
$archive->get('/{year}/', 'homepage.controller:getYearPage')->bind('archive-home')->assert('year', '\d+');
$archive->get('/{year}/gallery/backstage/', 'gallery.controller:getBackstageGallery')->bind('gallery.day')->assert('year', '\d+');
$archive->get('/{year}/gallery/{day}/', 'gallery.controller:getGallery')->bind('gallery.day')->assert('year', '\d+');
$archive->get('/{year}/gallery/', 'gallery.controller:getGallery')->bind('gallery')->assert('year', '\d+');
$archive->get('/{year}/videos/', 'gallery.controller:getVideos')->bind('videos')->assert('year', '\d+');
$app->mount('/', $archive);

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code, 'title' => $code, 'year' => 2018, 'years' => [2018])), $code);
});
