<?php

namespace App\Controllers;

use Silex\Application;

class AboutController extends BaseController
{
	public function getPressPage()
	{
		return $this->render('press.html.twig', [
			'title' => 'Press'
		]);
	}

	public function getStoryPage()
	{
		return $this->render('story.html.twig', [
			'title' => 'Story'
		]);
	}

	public function getWhoWeHelpPage()
	{
		$gallery1 = $this->buildImageList('/gal/toys-r-us-2018/');
		$gallery2 = $this->buildImageList('/gal/teds-shack/');

		return $this->render('who-we-help.html.twig', [
			'title' => 'Who We Help',
			'gallery1' => $gallery1,
			'gallery2' => $gallery2
		]);
	}

	private function buildImageList($baseGalleryPath)
	{
		$absPath = __DIR__.'/../../web'.$baseGalleryPath;
		$list = [];
		$handle = opendir($absPath);
		while (($file = readdir($handle)) !== false) {

			if ($file[0] === '.' || $file[0] === '_') {
				continue;
			}

			if (is_dir($absPath . $file)) {
				continue;
			}

			$list[] = [
				'full' => $baseGalleryPath.$file,
				'thumb' => $baseGalleryPath.'/thumbs/'.$file,
			];
		}

		return $list;
	}

	public function getMerchandise()
	{
		return $this->render('merch.html.twig', [
			'title' => 'Merchandise'
		]);
	}
}