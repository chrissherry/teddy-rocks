<?php

namespace App\Controllers;

use GuzzleHttp\Client;
use PDO;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class LineupController extends BaseController
{
	public function getClashFinder(Application $app)
	{
		return $this->render('lineup-tbc.html.twig', [
			'title' => 'Lineup',
		]);

		$now = new \DateTimeImmutable();
		$fridayStart = new \DateTimeImmutable('2017-05-04 17:00:00');
		$saturdayStart = new \DateTimeImmutable('2017-05-05 11:00:00');
		$sundayStart = new \DateTimeImmutable('2017-05-06 11:00:00');
		$festivalEnd = new \DateTimeImmutable('2017-05-06 23:59:00');

		return $this->render('clashfinder.html.twig', [
			'title' => 'Clashfinder',
			'now' => $now,
			'fridayStart' => $fridayStart,
			'saturdayStart' => $saturdayStart,
			'sundayStart' => $sundayStart,
			'festivalEnd' => $festivalEnd
		]);
	}

	public function getLineupJson()
	{
		$json = json_decode(file_get_contents(__DIR__.'/../../data/bands2018.json'));
		return new JsonResponse($json, 200);
	}

	public function getLineup(Application $app)
	{
		return $this->render('lineup-tbc.html.twig', [
			'title' => 'Lineup',
		]);

		$lineups = json_decode(file_get_contents(__DIR__.'/../../data/bands2018.json'), true);

		return $this->render('lineup.html.twig', [
			'title' => 'Lineup',
			'lineup' => $lineups
		]);
	}

	public function saveFavourite(Request $request)
	{
		//todo: check is ajax.

		$band = $request->request->get('band');

		if(!$band) {
			return new JsonResponse([], 400);
		}
		$bandname = strtolower(str_replace(' ', '_', $band));

		try{
			$pdo = $this->_dbconnect();

			$statement = $pdo->prepare('select * from favourites where bandname = ?');
			$statement->execute([$bandname]);
			$band = $statement->fetch();

			if($band) {
				$statement = $pdo->prepare('update favourites set favourites = favourites + 1 where bandname = ?');
				$statement->execute([$bandname]);
			} else {
				$statement = $pdo->prepare('insert into favourites (bandname) VALUES (?)');
				$statement->execute([$bandname]);
			}

		} catch (\Exception $e){
			error_log($e);
		}

		return new JsonResponse([], 200);
	}

	private function _dbconnect()
	{
		//todo: swap this out for a proper doctrine implemenation

		$host = 'localhost';
		$db   = 'teddyrocks';
		$user = 'teddyrocks';
		$pass = 'pleasechangethispassword';
		$charset = 'utf8';

		$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
		$opt = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		return new PDO($dsn, $user, $pass, $opt);
	}
}