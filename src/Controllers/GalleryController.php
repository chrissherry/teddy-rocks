<?php

namespace App\Controllers;

use Google_Client;
use Google_Service_YouTube;
use Google_Service_YouTube_PlaylistItem;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class GalleryController extends BaseController
{
	public function getGallery(Application $app, Request $request, $day = null)
	{

		$baseGalleryPath = '/gal/'.$this->getYear().'/';
		if($day){
			$baseGalleryPath .= $day.'/';
		}
		$baseGalleryAbsPath = __DIR__.'/../../web'.$baseGalleryPath;

		if(!file_exists($baseGalleryAbsPath)){
			$app->abort(404, "No gallery exists for this year");
		}

		$galleries = $this->buildGallery($baseGalleryAbsPath);

		return $this->render('gallery.html.twig', [
			'title' => 'Gallery',
			'baseGalleryPath' => $baseGalleryPath,
			'galleries' => $galleries,
			'year' => $this->getYear(),
			'day' => $day,
			'skipSponsors' => true
		]);
	}

	public function getBackstageGallery(Application $app, Request $request, $day = null)
	{
		$baseGalleryPath = '/gal/'.$this->getYear().'/backstage/';
		if($day){
			$baseGalleryPath .= $day.'/';
		}
		$baseGalleryAbsPath = __DIR__.'/../../web'.$baseGalleryPath;

		if(!file_exists($baseGalleryAbsPath)){
			$app->abort(404, "No gallery exists for this year");
		}

		$galleries = $this->buildGallery($baseGalleryAbsPath);

		return $this->render('gallery.html.twig', [
			'title' => 'Gallery',
			'baseGalleryPath' => $baseGalleryPath,
			'galleries' => $galleries,
			'year' => $this->getYear(),
			'day' => $day,
			'skipSponsors' => true,
			'backstage' => true
		]);
	}

	private function buildGallery($baseGalleryAbsPath)
	{
		$photographers = $this->getPhotographers();
		shuffle($photographers);

		foreach($photographers as &$photographer) {

			$imagePath = $baseGalleryAbsPath.$photographer['id'].'/';

			if(!file_exists($imagePath)){
				continue;
			}

			if (is_dir($imagePath)) {
				$handle = opendir($imagePath);
				$directoryfiles = array();
				while (($file = readdir($handle)) !== false) {

					if ($file[0] === '.' || $file[0] === '_') {
						continue;
					}

					if (is_dir($imagePath . $file)) {
						continue;
					}

					$newfile = str_replace(' ', '_', $file);
					rename($imagePath . $file, $imagePath . $newfile);
					rename($imagePath .'thumbs/'. $file, $imagePath .'thumbs/'. $newfile);
					$directoryfiles[] = $file;
				}

				foreach ($directoryfiles as $directoryfile) {
					$photographer['images'][] = $directoryfile;
				}

				if($photographer['sort'] === true) {
					usort($photographer['images'], function ($a, $b) {
						preg_match_all('!\d+!', $a, $matches);
						if (!isset($matches[0])) {
							return 0;
						}
						$a_number = $matches[0][count($matches[0]) - 1];

						preg_match_all('!\d+!', $b, $matches);
						if (!isset($matches[0])) {
							return 0;
						}
						$b_number = $matches[0][count($matches[0]) - 1];

						if ($a_number == $b_number) {
							return 0;
						}
						return ($a_number < $b_number) ? -1 : 1;

					});
				}

				closedir($handle);
			}
		}

		return $photographers;
	}

	private function getPhotographers()
	{
		return [
			[
				'id' => 'ss',
				'name' => 'Sebastian Schofield',
				'link' => 'http://sebastianschofieldphotography.com',
				'contact_link' => 'http://sebastianschofieldphotography.com/?page_id=2',
				'sort' => true,
				'images' => []
			],
			[
				'id' => 'cr',
				'name' => 'Charlie Raven',
				'link' => 'http://www.charlieraven.com',
				'contact_link' => 'http://www.charlieraven.com/contact',
				'logo' => '/img/sponsors/charlieraven.png',
				'sort' => true,
				'images' => []
			],
			[
				'id' => 'rc',
				'name' => 'Russ Collins',
				'link' => 'https://www.facebook.com/pg/RussCollinsPhotography/',
				'contact_link' => 'https://www.facebook.com/pg/RussCollinsPhotography/',
				'sort' => true,
				'images' => []
			],
			[
				'id' => 'rm',
				'name' => 'Rhona Murphy',
				'link' => 'https://www.facebook.com/rhonamurphyphotography/',
				'contact_link' => 'https://www.facebook.com/rhonamurphyphotography/',
				'sort' => true,
				'images' => []
			],
			[
				'id' => 'sl',
				'name' => 'Sarah Lockwood',
				'link' => 'http://www.facebook.com/SarahLockwoodPhotography',
				'contact_link' => 'http://www.facebook.com/SarahLockwoodPhotography',
				'sort' => false,
				'images' => []
			],
			[
				'id' => 'jm',
				'name' => 'James Marshall',
				'link' => 'http://basementhire.co.uk/',
				'contact_link' => 'http://basementhire.co.uk/',
				'sort' => false,
				'images' => []
			],
			[
				'id' => 'shj',
				'name' => 'Stephen & Helen Jones',
				'sort' => false,
				'images' => []
			],
			[
				'id' => 'wb',
				'name' => 'Will Bailey',
				'link' => 'https://www.willbaileyonline.com',
				'sort' => false,
				'images' => []
			]
		];
	}

	public function getVideos(Application $app)
	{
		$playlists = [
			2015 => 'PL6CmXh_kG-xz3NY7Isp2u0HDDh-_0Fx-A',
			2016 => 'PL6CmXh_kG-xy9H2g8JaEjJyPw0Vwje4XX',
			2017 => 'PL6CmXh_kG-xz-5ihNk4jXv_7WnQKVXT3O'
		];

		if(!array_key_exists($this->getYear(), $playlists)) {
			$app->abort(404, "No video channel exists for this year");
		}

		$client = new Google_Client();
		$client->setApplicationName("Client_Library_Examples");
		$client->setDeveloperKey("AIzaSyC5OAvULj9EsSrMOpfJPbvx3_HQ-j1Ybvo");
		$youtube = new Google_Service_YouTube($client);

		try {
			$searchResponse = $youtube->playlistItems->listPlaylistItems('snippet', array(
				'playlistId' => $playlists[$this->getYear()],
				'maxResults' => 50,
				'fields' => 'items(snippet(publishedAt,channelId,title,description,thumbnails(default),resourceId)),pageInfo,nextPageToken'));


			$videos = array_map(function (Google_Service_YouTube_PlaylistItem $item) {

				$thumb = $item->getSnippet()->getThumbnails()->getMaxres();
				if(!$thumb) {
					$thumb = $item->getSnippet()->getThumbnails()->getDefault();
				}

				return [
					'id' => $item->getSnippet()->getResourceId()->getVideoId(),
					'thumb' => $thumb->getUrl(),
					'title' => $item->getSnippet()->getTitle(),
					'description' => $item->getSnippet()->getDescription(),
				];
			}, $searchResponse->getItems());
		} catch (\Exception $e) {
			echo $e->getMessage();exit;
		}

		return $this->render('videos.html.twig', [
			'title' => 'Video Channel',
			'skipSponsors' => true,
			'videos' => $videos
		]);

	}
}