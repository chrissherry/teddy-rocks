<?php

namespace App\Controllers;

class InfoController extends BaseController
{
	public function getInfoPage()
	{
		return $this->render('info.html.twig', [
			'title' => 'Info',
		]);
	}

	public function getGetInvolvedPage()
	{
		return $this->render('get-involved.html.twig', [
			'title' => 'Get involved',
		]);
	}

	public function getDonatePage()
	{
		return $this->render('donate.html.twig', [
			'title' => 'Donate',
		]);
	}
}