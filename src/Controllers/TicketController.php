<?php

namespace App\Controllers;

use Silex\Application;

class TicketController extends BaseController
{
	public function getTicketPage(Application $app)
	{
		if (new \DateTimeImmutable() > new \DateTimeImmutable('2019-05-03 08:00:00')) {
			return $this->render('tickets-on-the-door.html.twig', [
				'title' => 'Tickets'
			]);
		} elseif (new \DateTimeImmutable() < new \DateTimeImmutable('2018-10-05 09:00:00')) {
			return $this->render('tickets-soon.html.twig', [
				'title' => 'Tickets'
			]);
		} else {
			return $this->render('tickets.html.twig', [
				'title' => 'Tickets'
			]);
		}
	}

	public function getLocalsTicketPage(Application $app)
	{
		return $this->render('tickets-local.html.twig', [
			'title' => 'Tickets'
		]);
	}
}