<?php

namespace App\Controllers;

use Silex\Application;

class HomepageController extends BaseController
{
	public function getIndex(Application $app)
	{
		$now = new \DateTimeImmutable();
		$day = strtolower($now->format('l'));
		$live = $this->live($now);

		return $this->render('index.html.twig', [
			'title' => 'Home',
			'page' => 'homepage',
//			'live' => $live,
			'day' => $day
		]);
	}

	public function getYearPage()
	{
		return $this->render(sprintf('archives/%s.html.twig', $this->getYear()), [
			'title' => sprintf('Archive: Teddy Rocks %s', $this->getYear()),
			'page' => 'homepage'
		]);
	}

	private function live($now)
	{
		$day = strtolower($now->format('l'));

		$bands = file_get_contents(__DIR__.'/../../data/bands2018.json');
		$bands = json_decode($bands, true);

		$live = [];

		if(!array_key_exists($day, $bands)) {
			return $live;
		}

		foreach($bands[$day] as $stagename => $stageinfo){

			foreach($stageinfo['bands'] as $bandname => $bandinfo){

				if($bandname === 'newbands') {
					$bandinfo = reset($bandinfo);
				}

				$startdate = null;
				$start = $bandinfo['start'];
				$hr =  substr($start,0,2);
				$min = substr($start,2);
				$time = $hr.':'.$min.':00';
				$datetime = $now->format('Y-m-d').$time;
				$startdate = new \DateTimeImmutable($datetime);
				$enddate = null;
				$end = $bandinfo['end'];
				$hr =  substr($end,0,2);
				$min = substr($end,2);
				$time = $hr.':'.$min.':00';
				$datetime = $now->format('Y-m-d').$time;
				$enddate = new \DateTimeImmutable($datetime);

				if($startdate < $now && $enddate > $now)
				{
					if(file_exists(__DIR__.'/../../web/img/live/'.strtolower(str_replace(' ','_',$bandname)).'.jpg')) {
						$img = '/img/live/' . strtolower(str_replace(' ', '_', $bandname)) . '.jpg';
					}
					else if(file_exists(__DIR__.'/../../web/img/live/'.strtolower(str_replace(' ','_',$bandname)).'.gif')){
						$img = '/img/live/'.strtolower(str_replace(' ','_',$bandname)).'.gif';
					} else {
						$img = '/img/live-stages/'.$stagename.'.jpg';
					}

					$live[$stagename] = ['name' => $bandname, 'image' => $img];
					break;
				}

			}
		}

		return $live;
	}
}