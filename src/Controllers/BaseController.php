<?php

namespace App\Controllers;

use Silex\Application;

class BaseController
{
	/** Years that the festival has run for
	 *
	 * @var array
	 */
	private $years = [
		2019,
		2018,
		2017,
		2016,
		2015,
		2014,
		2013,
		2012,
		2011
	];

	/** @var int */
	private $year = 2019;

	/** @var int */
	private $activeYear = 2019;

	function __construct(Application $app)
	{
		$this->app = $app;

		$segments = explode('/', $_SERVER['REQUEST_URI']);
		$yearSegment = $segments[1];
		if (is_numeric($yearSegment)) {
			if(in_array((int) $yearSegment, $this->years, true)) {
				$this->year = $yearSegment;
			}
		}
	}

	public function getYear()
	{
		return $this->year;
	}

	public function getYears()
	{
		return $this->years;
	}

	public function render($viewName, array $data)
	{
		$data = array_merge($data, [
			'years' => $this->years,
			'year' => $this->getYear(),
			'activeYear' => $this->activeYear,
			'isArchive' => $this->isArchive()
		]);

		return $this->app['twig']->render($viewName, $data);
	}

	public function isArchive()
	{
		return $this->year !== $this->activeYear;
	}
}