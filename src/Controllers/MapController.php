<?php

namespace App\Controllers;

use Pusher\Pusher;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MapController extends BaseController
{
	public function getMap(Application $app)
	{
		return $this->render('map/map.html.twig', [
			'title' => 'Map',
			'id' => uniqid(),
			'sendPosition' => true
		]);
	}

	public function getBackstageMap(Application $app)
	{
		return $this->render('map/map.html.twig', [
			'title' => 'Map',
			'id' => uniqid(),
			'backstage' => true,
			'sendPosition' => true
		]);
	}

	public function getMapWatch(Application $app)
	{
		return $this->render('map/map.html.twig', [
			'title' => 'Map',
			'isAdmin' => true
		]);
	}

	public function sendEvent(Request $request)
	{
		$lat = $request->request->get('lat');
		$lng = $request->request->get('lng');
		$id = $request->request->get('id');
		$name = $request->request->get('name');

		if (!$name) {
			$name = substr($id, 0, 4);
		}

		$options = array(
			'cluster' => 'eu',
			'encrypted' => true
		);
		$pusher = new Pusher(
			'bc5351d0cb0dddf46b02',
			'c80f72c49541f3a5f0ef',
			'501545',
			$options
		);

		$data['lat'] = $lat;
		$data['lng'] = $lng;
		$data['id'] = $id;
		$data['name'] = $name;
		$pusher->trigger('user-'.$id, 'bear-move', $data);
		$pusher->trigger('master', 'bear-move', $data);

		$txt = sprintf('%s,%s,%s,%s', date('Y-m-d H:i:s'), $id, $lat, $lng);
		file_put_contents(__DIR__.'/../../locations.csv', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);

		return new JsonResponse($data, 200);
	}
}