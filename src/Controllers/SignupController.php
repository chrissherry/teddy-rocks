<?php

namespace App\Controllers;

use Mailgun\Mailgun;
use PDO;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class SignupController extends BaseController
{
	/** @var Mailgun */
	private $mailgun;

	public function __construct(Application $app)
	{
		parent::__construct($app);

		$this->mailgun = Mailgun::create('key-dd4e6efbefc712bf0a223e2cd91bc988');
	}

	public function getSignupPlayPage()
	{
		return $this->render('signups/play.html.twig', [
			'title' => 'Play at Teddy Rocks Festival 2018',
		]);
	}

	public function getSignupVolunteerPage()
	{
		return $this->render('signups/volunteer.html.twig', [
			'title' => 'Volunteer at Teddy Rocks Festival 2018',
		]);
	}

	public function getSignupTradePage()
	{
		return $this->render('signups/trade.html.twig', [
			'title' => 'Trade at Teddy Rocks Festival 2018',
		]);
	}

	public function getSignupSponsorPage()
	{
		return $this->render('signups/sponsor.html.twig', [
			'title' => 'Sponsor Teddy Rocks Festival 2018',
		]);
	}

	private function _dbconnect()
	{
		//todo: swap this out for a proper doctrine implemenation

		$host = 'localhost';
		$db   = 'teddyrocks';
		$user = 'teddyrocks';
		$pass = 'pleasechangethispassword';
		$charset = 'utf8';

		$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
		$opt = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		return new PDO($dsn, $user, $pass, $opt);
	}

	public function signupPlay(Request $request)
	{
		try{
			$pdo = $this->_dbconnect();

			$statement = $pdo->prepare('insert into artists (act_type, name, location, genre1, genre2, genre3, link, musiclink, hook, contactname, contactemail, contactphone, date) VALUES (:act_type, :name, :location, :genre1, :genre2, :genre3, :link, :musiclink, :hook, :contactname, :contactemail, :contactphone, NOW())');

			$statement->execute([
				':act_type' => $request->request->get('act_type', 'Original'),
				':name' => $request->request->get('name', ''),
				':location' => $request->request->get('location', ''),
				':genre1' => $request->request->get('genre1', ''),
				':genre2' => $request->request->get('genre2', ''),
				':genre3' => $request->request->get('genre3', ''),
				':link' => $request->request->get('link', ''),
				':musiclink' => $request->request->get('musiclink', ''),
				':hook' => $request->request->get('hook', ''),
				':contactname' => $request->request->get('contactname', ''),
				':contactemail' => $request->request->get('contactemail', ''),
				':contactphone' => $request->request->get('contactphone', '')
			]);

		} catch (\Exception $e){
			error_log($e);
			return $this->render('signups/play.html.twig', [
				'title' => 'Play at Teddy Rocks Festival 2018',
				'error' => $e->getMessage()
			]);
		}

		try {
			$this->mailgun->messages()->send('teddyrocks.co.uk', [
				'from'    => 'no-reply@teddyrocks.co.uk',
				'to'      => 'chris@teddyrocks.co.uk',
				'subject' => 'New Artist: Teddy Rocks 2018',
				'text'    => "A new artist has signed up for this year's festival.\n\n Download the latest list of artists for here: https://www.teddyrocks.co.uk/admin/artists"
			]);
		} catch (\Exception $e) {
			error_log($e);
			// silence is golden
		}

		return $this->render('signups/play.html.twig', [
			'title' => 'Play at Teddy Rocks Festival 2018',
			'complete' => true
		]);
	}

	public function signupVolunteer(Request $request)
	{
		try{
			$pdo = $this->_dbconnect();

			$statement = $pdo->prepare('insert into volunteers (contactname, contactemail, contactphone, support_details, date) VALUES (:contactname, :contactemail, :contactphone, :support_details, NOW())');

			$statement->execute([
				':contactname' => $request->request->get('contactname', ''),
				':contactemail' => $request->request->get('contactemail', ''),
				':contactphone' => $request->request->get('contactphone', ''),
				':support_details' => $request->request->get('support_details', '')
			]);

		} catch (\Exception $e){
			error_log($e);
			return $this->render('signups/volunteer.html.twig', [
				'title' => 'Volunteer at Teddy Rocks Festival 2018',
				'error' => $e->getMessage()
			]);
		}

		try {
			$this->mailgun->messages()->send('teddyrocks.co.uk', [
				'from'    => 'no-reply@teddyrocks.co.uk',
				'to'      => 'chris@teddyrocks.co.uk',
				'subject' => 'New Volunteer: Teddy Rocks 2018',
				'text'    => "A new volunteer has signed up for this year's festival.\n\n Download the latest list of volunteers for here: https://www.teddyrocks.co.uk/admin/volunteers"
			]);
		} catch (\Exception $e) {
			error_log($e);
			// silence is golden
		}

		return $this->render('signups/volunteer.html.twig', [
			'title' => 'Volunteer at Teddy Rocks Festival 2018',
			'complete' => true
		]);
	}

	public function signupTrade(Request $request)
	{
		try{
			$pdo = $this->_dbconnect();

			$statement = $pdo->prepare('insert into traders (tradename, about, contactname, contactemail, contactphone, requirements, date) VALUES (:tradename, :about, :contactname, :contactemail, :contactphone, :requirements, NOW())');

			$statement->execute([
				':tradename' => $request->request->get('tradename', ''),
				':about' => $request->request->get('about', ''),
				':contactname' => $request->request->get('contactname', ''),
				':contactemail' => $request->request->get('contactemail', ''),
				':contactphone' => $request->request->get('contactphone', ''),
				':requirements' => $request->request->get('requirements', '')
			]);

		} catch (\Exception $e){
			error_log($e);
			return $this->render('signups/trade.html.twig', [
				'title' => 'Trade at Teddy Rocks Festival 2018',
				'error' => $e->getMessage()
			]);
		}

		try {
			$this->mailgun->messages()->send('teddyrocks.co.uk', [
				'from'    => 'no-reply@teddyrocks.co.uk',
				'to'      => 'tom@teddyrocks.co.uk',
				'subject' => 'New Trader: Teddy Rocks 2018',
				'text'    => "A new trader has signed up for this year's festival.\n\n Download the latest list of traders for here: https://www.teddyrocks.co.uk/admin/traders"
			]);
		} catch (\Exception $e) {
			error_log($e);
			// silence is golden
		}

		return $this->render('signups/trade.html.twig', [
			'title' => 'Trade at Teddy Rocks Festival 2018',
			'complete' => true
		]);
	}

	public function signupSponsor(Request $request)
	{
		try{
			$pdo = $this->_dbconnect();

			$statement = $pdo->prepare('insert into sponsors (contactname, contactemail, contactphone, sponsor_type, message, date) VALUES (:contactname, :contactemail, :contactphone, :sponsor_type, :message, NOW())');

			$statement->execute([
				':contactname' => $request->request->get('contactname', ''),
				':contactemail' => $request->request->get('contactemail', ''),
				':contactphone' => $request->request->get('contactphone', ''),
				':sponsor_type' => $request->request->get('sponsor_type', ''),
				':message' => $request->request->get('message', '')
			]);

		} catch (\Exception $e){
			error_log($e);
			return $this->render('signups/sponsor.html.twig', [
				'title' => 'Sponsor Teddy Rocks Festival 2018',
				'error' => $e->getMessage()
			]);
		}

		try {
			$this->mailgun->messages()->send('teddyrocks.co.uk', [
				'from'    => 'no-reply@teddyrocks.co.uk',
				'to'      => 'tom@teddyrocks.co.uk',
				'subject' => 'New Sponsor: Teddy Rocks 2018',
				'text'    => "A new sponsor has signed up for this year's festival.\n\n Download the latest list of sponsors for here: https://www.teddyrocks.co.uk/admin/sponsors"
			]);
		} catch (\Exception $e) {
			error_log($e);
			// silence is golden
		}

		return $this->render('signups/sponsor.html.twig', [
			'title' => 'Sponsor Teddy Rocks Festival 2018',
			'complete' => true
		]);
	}

	public function getPlayList(Request $request)
	{
		$offset = $request->request->get('after', 0);

		try {
			$pdo = $this->_dbconnect();

			$statement = $pdo->prepare('select * from artists where id > ?');
			$statement->execute([$offset]);
			$artists = $statement->fetchAll();

			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=artists.csv");
			header("Pragma: no-cache");
			header("Expires: 0");

			echo "Id, Act Type, Name, Location, Genre 1, Genre 2, Genre 3, Link, Link to Music, What makes you stand out, Contact Name, Contact Email, Contact Phone, Date \r\n";

			foreach($artists as $artist)
			{
				foreach($artist as $aData) {
					$echo =  str_replace(',','.',$aData);
					$echo = trim(preg_replace('/\s\s+/', ' '
						, $echo));
					echo $echo.',';;
				}
				echo "\r\n";
			}
			exit;

		} catch (\Exception $e){
			error_log($e);
			throw new \Exception('Sorry, something went wrong');
		}
	}

	public function getVolunteerList(Request $request)
	{
		$offset = $request->request->get('after', 0);

		try {
			$pdo = $this->_dbconnect();

			$statement = $pdo->prepare('select * from volunteers where id > ?');
			$statement->execute([$offset]);
			$volunteers = $statement->fetchAll();

			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=volunteers.csv");
			header("Pragma: no-cache");
			header("Expires: 0");

			echo "Id, Contact Name, Contact Email, Contact Phone, Medical / Support Needs, Date \r\n";

			foreach($volunteers as $volunteer)
			{
				foreach($volunteer as $aData) {
					$echo =  str_replace(',','.',$aData);
					$echo = trim(preg_replace('/\s\s+/', ' '
						, $echo));
					echo $echo.',';;
				}
				echo "\r\n";
			}
			exit;

		} catch (\Exception $e){
			error_log($e);
			throw new \Exception('Sorry, something went wrong');
		}
	}

	public function getTraderList(Request $request)
	{
		$offset = $request->request->get('after', 0);

		try {
			$pdo = $this->_dbconnect();

			$statement = $pdo->prepare('select * from traders where id > ?');
			$statement->execute([$offset]);
			$volunteers = $statement->fetchAll();

			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=traders.csv");
			header("Pragma: no-cache");
			header("Expires: 0");

			echo "Id, Trader Name, About, Contact Name, Contact Email, Contact Phone, Requirements, Date \r\n";
			foreach($volunteers as $volunteer)
			{
				foreach($volunteer as $aData) {
					$echo =  str_replace(',','.',$aData);
					$echo = trim(preg_replace('/\s\s+/', ' '
						, $echo));
					echo $echo.',';;
				}
				echo "\r\n";
			}
			exit;

		} catch (\Exception $e){
			error_log($e);
			throw new \Exception('Sorry, something went wrong');
		}
	}

	public function getSponsorList(Request $request)
	{
		$offset = $request->request->get('after', 0);

		try {
			$pdo = $this->_dbconnect();

			$statement = $pdo->prepare('select * from sponsors where id > ?');
			$statement->execute([$offset]);
			$volunteers = $statement->fetchAll();

			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=sponsors.csv");
			header("Pragma: no-cache");
			header("Expires: 0");

			echo "Id, Contact Name, Contact Email, Contact Phone, Type, Date \r\n";
			foreach($volunteers as $volunteer)
			{
				foreach($volunteer as $aData) {
					$echo =  str_replace(',','.',$aData);
					$echo = trim(preg_replace('/\s\s+/', ' '
						, $echo));
					echo $echo.',';;
				}
				echo "\r\n";
			}
			exit;

		} catch (\Exception $e){
			error_log($e);
			throw new \Exception('Sorry, something went wrong');
		}
	}
}