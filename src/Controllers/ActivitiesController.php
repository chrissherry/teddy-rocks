<?php

namespace App\Controllers;

use Silex\Application;

class ActivitiesController extends BaseController
{
	public function getActivitiesPage(Application $app)
	{
		return $this->render('activities.html.twig', [
			'title' => 'Activities'
		]);
	}
}