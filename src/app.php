<?php

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;

$client = new Raven_Client('https://1ad0ae3f2b3e42199554b9ac5f9618a0:1b3bf1deac894c68994ea770c001f09e@sentry.io/250437');
$client->install();

$app = new Application();

date_default_timezone_set("Europe/London");

$app->register(new Silex\Provider\SecurityServiceProvider(), [
	'security.firewalls' => [
		'admin' => array(
			'pattern' => '^/admin',
			'http' => true,
			'users' => array(
				'tom' => array('ROLE_ADMIN', '$2y$13$xdhm5dFLTZm1xv0F/I5EM.bs4zUnbBvTQcgfeXkPup3gr/ysr7/fG'),
			),
		),
		'local' => array(
			'pattern' => '^/tickets/local',
			'http' => true,
			'users' => array(
				'tom' => array('ROLE_ADMIN', '$2y$13$xdhm5dFLTZm1xv0F/I5EM.bs4zUnbBvTQcgfeXkPup3gr/ysr7/fG'),
			),
		),
		'volunteers' => array(
			'pattern' => '^/volunteers',
			'http' => true,
			'users' => array(
				'teddy' => array('ROLE_ADMIN', '$2y$13$Yra7M/g5IlaCa/visqhA4OV9KxpS0TH13TAajBhfjOqsZJVRDGyie'),
			),
		)
	]
]);

//$encoded = $app['security.default_encoder']->encodePassword('rocks', '');
//echo $encoded;exit;

$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider(), [
	'assets.named_packages' => array(
		'css' => [
			'version' => '104',
			'base_path' => '/compiled',
			'version_format' => '%s?v=%s'
		],
		'js' => [
			'version' => '9',
			'base_path' => '/js',
			'version_format' => '%s?v=%s'
		],
		'slick' => [
			'base_path' => '/slick',
		],
		'photoswipe' => [
			'base_path' => '/photoswipe',
		],
	),
]);
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app['twig'] = $app->extend('twig', function ($twig, Application $app) {
    return $twig;
});

return $app;
