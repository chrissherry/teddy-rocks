$( document ).ready(function() {

    var bgVideo = $('.bg-video');

    function togglePointer(){
        $('.emodjirock').toggle();
        $('.emodjidown').toggle();
        setTimeout(togglePointer, 1500);
    }

    if($(window).scrollTop() == 0) {
        if (bgVideo.length && bgVideo.is(':visible')) {
            setTimeout(function () {
                togglePointer();
            }, 35000);
        } else {
            setTimeout(function () {
                togglePointer();
            }, 5000);
        }
    }

    if (bgVideo.length && bgVideo.is(':visible')){
        $('.moving-header').hide();
        $('.strapline').hide();
        $('.totaliser').hide();

        offset = 3000;

        setTimeout(function(){
            $('.totaliser').fadeOut('slow');
        }, 10000);

        setTimeout(function(){
            $('.totaliser').fadeIn(1000);
        }, 3000);
    }

    sizeUp();

    $(window).on('resize', function () {
        sizeUp();
    });

    function sizeUp() {
        if($(window).width() > $(window).height()){
            videoHeight = $(window).height();
        } else {
            videoHeight = 600;
        }
        $('.container-header').addClass('container-video').css('height', videoHeight + 'px');
    }
});


