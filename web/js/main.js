// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

$( document ).ready(function() {

    var bgVideo = $('.bg-video');

    if (bgVideo.length && bgVideo.is(':visible')){

        $('.moving-header').hide();
        $('.strapline').hide();

        setTimeout(function(){
            $('.moving-header').fadeIn(1000);
            $('.strapline').fadeIn(1000);
        }, 3000);
    }

    if(!$('.no-move-header').length) {
        $('.moving-header').affix({
            offset: {
                top: videoHeight - 100
            }
        });
    }

    $('.lineup .moving-tabs').affix({
        offset: {
            top: 506
        }
    });

    $('.sponsors').slick({
        dots: false,
        infinite: true,
        speed: 300,
        autoplaySpeed: 1000,
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay:true,
        arrows:true,
        cssEase: 'linear',
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows:false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows:false
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    $('.moving-fix').on('affix.bs.affix', function(){
        var size = $(window).width(); //get browser width
        var divWidth = $('.container').width(); //get width of container
        var margin = (size - divWidth) / 2; //get difference and divide by 2
        $(".moving-tabs").css("right",margin).css("left",margin);
    }).on('affix-top.bs.affix', function () {
        $(".moving-tabs").css("right","0px").css("left","0px")
    });

    $('.moving-header').on('affix.bs.affix', function(){
        var size = $(window).width(); //get browser width
        var divWidth = $('.moving-header').width(); //get width of container
        var margin = (size - divWidth) / 2; //get difference and divide by 2
        $('.moving-header').css("right",margin).css("left",margin);
    }).on('affix-top.bs.affix', function () {
        $('.moving-header').css("right","0px").css("left","0px")
    });

    // Javascript to enable link to tab
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
        e.preventDefault();
    });

    $('.nav-tabs li a').click( function(e) {

        history.pushState( null, null, $(this).attr('href') );

    });

    $('.gallery').slick({
        dots: true,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows:true,
        cssEase: 'linear',
        infinite: false,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
});
