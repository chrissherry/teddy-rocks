#!/bin/bash

NOW=$(date +"%Y%m%d%H%M")
SITE_DIR=/var/www/teddyrocks
RELEASE_DIR=${SITE_DIR}/releases

mkdir ${RELEASE_DIR}/${NOW}

chown -R teddyrocks:www-data ${RELEASE_DIR}/${NOW}

tar -xzvf ~/release.tar.gz -C ${RELEASE_DIR}/${NOW}

rm ${SITE_DIR}/current
ln -s ${SITE_DIR}/shared/gal ${RELEASE_DIR}/${NOW}/web/gal
ln -s ${SITE_DIR}/shared/live ${RELEASE_DIR}/${NOW}/web/img/live
ln -s ${RELEASE_DIR}/${NOW} ${SITE_DIR}/current

# Remove all but the last 5 releases
# ls -dt ${RELEASE_DIR}/* | tail -n +5 | xargs rm -rf

sudo /usr/sbin/service php7.2-fpm reload
sudo /usr/sbin/service nginx reload