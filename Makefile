# Note: this needs tabs, not spaces
.PHONY:

deploy-full:

	tar --exclude='./var' --exclude='./web/gal' --exclude='./.git' -czf ~/release.tar.gz --exclude='./.idea' . && scp ~/release.tar.gz teddyrocks@46.101.36.189:/home/teddyrocks && ssh -T teddyrocks@46.101.36.189 <release.sh

deploy-quick:

	tar --exclude='./var' --exclude='./vendor' --exclude='./node_modules' --exclude='./web/gal' --exclude='./.git' --exclude='./.idea'  -czf ~/release.tar.gz . && scp ~/release.tar.gz teddyrocks@46.101.36.189:/home/teddyrocks && ssh -T teddyrocks@46.101.36.189 <release-quick.sh