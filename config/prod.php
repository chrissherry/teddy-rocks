<?php

// configure your app for the production environment
date_default_timezone_set("Europe/London");

$app['twig.path'] = array(__DIR__.'/../templates');
//$app['twig.options'] = array('cache' => __DIR__.'/../var/cache/twig');
