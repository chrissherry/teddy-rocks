# Open Source

* Move secrets into environment variables.
* Documentation
* Add database to dev environment

# Bugs / Tidy

* Background images
* Remove old css/js/images

# Reinstating old features

* Activities page
* FAQs page
* Map
* Clashfinder
* 'live' homepage feature

# New features

* Reviews page.
* Improved map with pusub

# Backlog

